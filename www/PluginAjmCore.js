var exec = require('cordova/exec');

var plugin = {
	getVersion: function(successCallback, errorCallback){
		exec(successCallback, errorCallback, 'PluginAjmCore', 'version', []);
	},
	
	getVersionJs: function(successCallback, errorCallback){
		successCallback("V1");
	}
};

module.exports = plugin;
