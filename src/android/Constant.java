package cordova.plugin.ajm.core.v1;

public final class Constant {
    public static final String PROPERTY_HASH = "hash";
    public static final String STATUS_OK = "OK";
    public static final String STATUS_ERROR = "ERROR";
    public static final String PROPERTY_STATUS = "status";
    public static final String PROPERTY_MESSAGE = "message";
    public static final String PROPERTY_USERID = "userId";
    public static final String UNKNOWN_MESSAGE = "UNKNOWN";
    public static final String PROPERTY_RESULT = "result";
}
