package cordova.plugin.ajm.core.v1;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Util {
    public static void send_NoResult(CallbackContext callbackContext, boolean keep){
        Util.send_Result(callbackContext, PluginResult.Status.NO_RESULT, keep, "");
    }

    public static void send_BadResult(CallbackContext callbackContext, boolean keep, String msg){
        Util.send_Result(callbackContext, PluginResult.Status.ERROR, keep, msg);
    }

    public static void send_Result(CallbackContext callbackContext, boolean keep, String msg){
        Util.send_Result(callbackContext, PluginResult.Status.OK, keep, msg);
    }

    public static void send_Result(CallbackContext callbackContext, PluginResult.Status status, boolean keep, String msg) {
        PluginResult pluginResult = new PluginResult(status, msg);
        pluginResult.setKeepCallback (keep);
        callbackContext.sendPluginResult(pluginResult);
    }

    public static void send_Result(CallbackContext callbackContext, boolean keep, JSONObject msg){
        Util.send_Result(callbackContext, PluginResult.Status.OK, keep, msg);
    }

    public static void send_BadResult(CallbackContext callbackContext, boolean keep, JSONObject msg){
        Util.send_Result(callbackContext, PluginResult.Status.ERROR, keep, msg);
    }

    public static void send_Result(CallbackContext callbackContext, PluginResult.Status status, boolean keep, JSONObject msg) {
        PluginResult pluginResult = new PluginResult(status, msg);
        pluginResult.setKeepCallback (keep);
        callbackContext.sendPluginResult(pluginResult);
    }

    public static <T> void addProperty(String id, T value, JSONObject item){
        try {

            if(item.has(id)){
                item.remove(id);
            }

            item.put(id, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getDefaultResponse(String status) {
        JSONObject item = new JSONObject();

        Util.addProperty(Constant.PROPERTY_STATUS, status, item);
        Util.addProperty(Constant.PROPERTY_MESSAGE, Constant.UNKNOWN_MESSAGE, item);

        return item;
    }

    public static JSONObject getDefaultOkResponse() {
        JSONObject item = new JSONObject();

        Util.addProperty(Constant.PROPERTY_STATUS, Constant.STATUS_OK, item);
        Util.addProperty(Constant.PROPERTY_MESSAGE, Constant.STATUS_OK, item);

        return item;
    }

    public static JSONObject getDefaultOkResponse(String message) {
        JSONObject item = new JSONObject();

        Util.addProperty(Constant.PROPERTY_STATUS, Constant.STATUS_OK, item);
        Util.addProperty(Constant.PROPERTY_MESSAGE, message, item);

        return item;
    }
}
