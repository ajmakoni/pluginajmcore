//
//  PAJC_Util.swift
//  
//
//  Created by Lester Quintero on 06/02/21.
//

import Foundation

public struct PAJC_Util {
        
    static func send_NoResult(commandDelegate: CDVCommandDelegate, callbackId: String, keep: Bool, message: String)
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_NO_RESULT, callbackId: callbackId, keep: keep, message: message)
    }
	
	static func send_NoResult(commandDelegate: CDVCommandDelegate, callbackId: String, keep: Bool)
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_NO_RESULT, callbackId: callbackId, keep: keep, message: "NO_RESULT")
    }
    
    static func send_BadResult(commandDelegate: CDVCommandDelegate, callbackId: String, keep: Bool, message: String)
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_ERROR, callbackId: callbackId, keep: keep, message: message)
    }
    
    static func send_BadResult(commandDelegate: CDVCommandDelegate, callbackId: String, message: String)
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_ERROR, callbackId: callbackId, keep: false, message: message)
    }
    
    static func send_Result(commandDelegate: CDVCommandDelegate, callbackId: String, keep: Bool, message: String)
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_OK, callbackId: callbackId, keep: keep, message: message)
    }
    
    static func send_Result(commandDelegate: CDVCommandDelegate, callbackId: String, message: String)
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_OK, callbackId: callbackId, keep: false, message: message)
    }
    
    static func send_Result(commandDelegate: CDVCommandDelegate, status: CDVCommandStatus, callbackId: String, keep: Bool, message: String) {
        
        let pluginResult = CDVPluginResult(status: status, messageAs: message)
        pluginResult?.setKeepCallbackAs(keep)
        
        commandDelegate.send(pluginResult, callbackId: callbackId)
    }
    
    static func send_BadResult(commandDelegate: CDVCommandDelegate, callbackId: String, keep: Bool, message: [String:Any])
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_ERROR, callbackId: callbackId, keep: keep, message: message)
    }
    
    static func send_BadResult(commandDelegate: CDVCommandDelegate, callbackId: String, message: [String:Any])
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_ERROR, callbackId: callbackId, keep: false, message: message)
    }
    
    static func send_Result(commandDelegate: CDVCommandDelegate, callbackId: String, keep: Bool, message: [String:Any])
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_OK, callbackId: callbackId, keep: keep, message: message)
    }
    
    static func send_Result(commandDelegate: CDVCommandDelegate, callbackId: String, message: [String:Any])
    {
        self.send_Result(commandDelegate: commandDelegate, status: CDVCommandStatus_OK, callbackId: callbackId, keep: false, message: message)
    }
    
    static func send_Result(commandDelegate: CDVCommandDelegate, status: CDVCommandStatus, callbackId: String, keep: Bool, message: [String:Any]) {
        
        var pluginResult = CDVPluginResult(status: status, messageAs: message)
        pluginResult?.setKeepCallbackAs(keep)
        
        commandDelegate.send(pluginResult, callbackId: callbackId)
    }
    
    static func getDefaultOkResponse() -> [String:Any]    {
        var result: [String:Any] = [
			PAJC_Constant.PROPERTY_STATUS:PAJC_Constant.STATUS_OK, 
			PAJC_Constant.PROPERTY_MESSAGE: PAJC_Constant.STATUS_OK
		]

        return result
    }
    
    static func getDefaultOkResponse(message : String) -> [String:Any]    {
        var result: [String:Any] = [
			PAJC_Constant.PROPERTY_STATUS:PAJC_Constant.STATUS_OK, 
			PAJC_Constant.PROPERTY_MESSAGE: message
		]

        return result
    }
    
    static func getDefaultResponse(status : String) -> [String:Any] {
        var result: [String:Any] = [
			PAJC_Constant.PROPERTY_STATUS:status, 
			PAJC_Constant.PROPERTY_MESSAGE: PAJC_Constant.UNKNOWN_MESSAGE
		]

        return result
    }
}
