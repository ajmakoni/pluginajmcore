//
//  PluginAjmCore_Constant.swift
//  
//
//  Created by Lester Quintero on 06/02/21.
//

import Foundation

public struct PAJC_Constant {
    static let PROPERTY_HASH = "hash"
    static let STATUS_OK = "OK";
    static let STATUS_ERROR = "ERROR"
    static let PROPERTY_STATUS = "status"
    static let PROPERTY_MESSAGE = "message"
    static let PROPERTY_USERID = "userid"
    static let PROPERTY_RESULT = "result"
    static let UNKNOWN_MESSAGE = "UNKNOWN"
}