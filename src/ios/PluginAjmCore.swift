//
//  PluginAjmCore.swift
//  
//
//  Created by Lester Quintero on 06/02/21.
//

import Foundation

@objc(PluginAjmCore) public class PluginAjmCore : CDVPlugin {

	@objc(version:) func version(_ command: CDVInvokedUrlCommand) {
		let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "V1")
		self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
	}
}